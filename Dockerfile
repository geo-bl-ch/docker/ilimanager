ARG ILIMANAGER_VERSION=f6b232e

FROM openjdk:8-alpine AS builder

RUN apk --update add git

WORKDIR /src

RUN git clone https://github.com/claeis/ilimanager.git && \
    cd ilimanager && \
    git checkout ${ILIMANAGER_VERSION} && \
    ./gradlew build && \
    mkdir /opt/ilimanager && \
    unzip dist/ilimanager*.zip -d /opt/ilimanager

FROM openjdk:22-slim

COPY --from=builder /opt/ilimanager /opt/ilimanager
COPY ilimanager /usr/local/bin/